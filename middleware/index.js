// all the middleare goes here
var middlewareObj = {};

middlewareObj.checkAdmin = function(req, res, next) {
  if(req.isAuthenticated()){
    if( req.user.role === 0) {
      next();
    } else {
      req.flash("error", "You don't have permission to do that");
      res.redirect("back");
    }
  } else {
    req.flash("error", "You need to be logged in to do that");
    res.redirect("back");
  }
}

middlewareObj.checkCenterAdmin = function(req, res, next) {
  if(req.isAuthenticated()){
    if( req.user.role === 2) {
      next();
    } else {
      req.flash("error", "You don't have permission to do that");
      res.redirect("back");
    }
  } else {
    req.flash("error", "You need to be logged in to do that");
    res.redirect("back");
  }
}

middlewareObj.checkQual = function(req, res, next) {
  if(req.isAuthenticated()){
    if( req.user.role === 3 && req.user.flag === 1 ) {
      next();
    } else {
      req.flash("error", "You don't have permission to do that");
      res.redirect("back");
    }
  } else {
    req.flash("error", "You need to be logged in to do that");
    res.redirect("back");
  }
}

middlewareObj.isLoggedIn = function(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error", "You need to be logged in to do that");
    res.redirect("/login");
}

module.exports = middlewareObj;
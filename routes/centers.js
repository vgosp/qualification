var express = require("express");
var router  = express.Router();
var mysql   = require("mysql");
var middleware = require("../middleware");

var connection = mysql.createPool({
  connectionLimit: 50,
  host: "localhost",
  user: "root",
  password: "",
  database: "db_project"
})

router.get("/",middleware.checkCenterAdmin, function(req, res){
      res.render("centers");    
});

router.post("/",middleware.checkAdmin,function(req,res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      var cname = req.body.name;
      var address = req.body.address;
      var id = req.body.adminID;
      tempConn.query("INSERT INTO examcenter ( centername, address, adminID) VALUES ( ?, ?, ? )",[cname, address, id],function(err,rows){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.redirect("centers/new");    
        }
      })
    }
  })
})

router.get("/new",middleware.checkAdmin, function(req, res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT * FROM examcenter",function(err,rows,fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.query("SELECT * FROM users WHERE role = ?",[2],function(err,users,fields){
            if(err){
              tempConn.release();
              console.log(err);
              req.flash("error", "Something went wrong");
              res.redirect("back");
            } else{
              tempConn.release();
              res.render("centers/new",{center: rows, users: users});    
            }
          })  
        }
      })
    }
  })
})

module.exports = router;
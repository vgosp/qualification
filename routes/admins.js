var express = require("express");
var router  = express.Router();
var mysql   = require("mysql");
var middleware = require("../middleware");

var connection = mysql.createPool({
  connectionLimit: 50,
  host: "localhost",
  user: "root",
  password: "",
  database: "db_project"
})

router.get("/",middleware.checkAdmin, function(req, res){
  res.render("administrators");    
});

router.post("/",middleware.checkAdmin,function(req,res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      var username = req.body.name;
      tempConn.query("INSERT INTO users (name , username, password, role) VALUES (?, ?, ?, ?)",[username,username,"password",2],function(err,rows){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.redirect("admins/new");    
        }
      })
    }
  })
})

router.get("/new",middleware.checkAdmin, function(req, res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT * FROM users WHERE role=?",[2],function(err,rows){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.render("administrators/new",{user: rows});    
        }
      })
    }
  })
})



module.exports = router;
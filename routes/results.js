var express = require("express");
var router  = express.Router();
var mysql   = require("mysql");
var middleware = require("../middleware");

var connection = mysql.createPool({
  connectionLimit: 50,
  host: "localhost",
  user: "root",
  password: "",
  database: "db_project"
})



router.get("/json", function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT * FROM answers",function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.json({results: rows});    
	        }
	      })
	    }
	})   
});

router.get("/json/:id", function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT * FROM answers WHERE centerID = ?",[req.params.id],function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.json({results: rows});    
	        }
	      })
	    }
	})   
});

router.get("/json/:id/:date", function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT * FROM answers WHERE centerID = ? AND date = ? ",[req.params.id,req.params.date],function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.json({results: rows});    
	        }
	      })
	    }
	})   
});

router.get("/json/candidates/:candID/r", function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT questionID, correct, answer, time FROM answers WHERE userID = ? ",[req.params.candID],function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.json({results: rows});    
	        }
	      })
	    }
	})   
});



router.get("/",middleware.checkAdmin, function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT * FROM answers",function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.render("results", {results: rows});    
	        }
	      })
	    }
	})   
});

router.get("/candidates/:candID/r",middleware.checkAdmin, function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT questionID, correct, answer, time FROM answers WHERE userID = ? ",[req.params.candID],function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.render("results/candidate", {results: rows});    
	        }
	      })
	    }
	})   
});

router.get("/:id",middleware.checkAdmin, function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT * FROM answers WHERE centerID = ?",[req.params.id],function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.render("results", {results: rows});    
	        }
	      })
	    }
	})   
});

router.get("/:id/:date",middleware.checkAdmin, function(req, res){
	connection.getConnection(function(err, tempConn){
	    if(err){
	      tempConn.release();
	      console.log(err);
	    } else {
	      tempConn.query("SELECT * FROM answers WHERE centerID = ? AND date = ? ",[req.params.id,req.params.date],function(err,rows,fields){
	        if(err){
	          tempConn.release();
	          console.log(err);
	        } else{
	          tempConn.release();
	          res.render("results", {results: rows});    
	        }
	      })
	    }
	})   
});

module.exports = router;
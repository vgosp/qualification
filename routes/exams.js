var express = require("express");
var router  = express.Router();
var mysql   = require("mysql");
var middleware = require("../middleware");

var connection = mysql.createPool({
  connectionLimit: 50,
  host: "localhost",
  user: "root",
  password: "",
  database: "db_project"
})

router.get("/", function(req, res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT centername FROM examcenter WHERE  adminID = ?",[ req.user.id ],function(err, rows, fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else {
          tempConn.query("SELECT * FROM examination WHERE  examcenter = ? and flag = ?",[ rows[0].centername, 0 ],function(err, rows, fields){
            if(err){
              tempConn.release();
              console.log(err);
              req.flash("error", "Something went wrong");
              res.redirect("back");
            } else {
              tempConn.release();
              res.render("exams",{exams: rows});
            }
          })
        }
      })
    }
  })
    
});

router.post("/",middleware.checkAdmin,function(req,res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      var center =req.body.center;
      var date =req.body.date;
      tempConn.query("INSERT INTO examination (examcenter, tdate, flag) VALUES ( ?, ?, ?)",[ center, date, 0],function(err, rows, fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.redirect("exams/new");    
        }
      })
    }
  })
})

router.get("/new",middleware.checkAdmin, function(req, res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT * FROM examination",function(err,rows){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          var examRows= rows
          tempConn.query("SELECT * FROM examcenter",function(err,rows){
            if(err){
              tempConn.release();
              console.log(err);
              req.flash("error", "Something went wrong");
              res.redirect("back");
            } else {
              tempConn.release();
              res.render("exams/new",{exam: examRows,center: rows});  
            }
          })
            
        }
      })
    }
  })
})

router.post("/start",middleware.checkCenterAdmin,function(req,res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("UPDATE examination SET flag = ? WHERE id = ? ",[ 1, req.body.id],function(err, rows, fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.redirect("/centers");    
        }
      })
    }
  })
})

router.post("/answers",middleware.checkQual,function(req,res){
  var date = new Date();
  var hours = date.getHours();
  var mins = date.getMinutes();
  var secs = date.getSeconds();
  var time = hours+":"+mins+":"+secs;
  date = date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear();
  var userID = req.user.id;
  var centerID = 15;
  var userID = req.user.id;
  var userID = req.user.id;
  var arr = [[userID,
            centerID,
            req.body.id0,
            req.body.correct0,
            req.body.answer0,
            date,
            time],
            [userID,
            centerID,
            req.body.id1,
            req.body.correct1,
            req.body.answer1,
            date,
            time],
            [userID,
            centerID,
            req.body.id2,
            req.body.correct2,
            req.body.answer2,
            date,
            time],
            [userID,
            centerID,
            req.body.id3,
            req.body.correct3,
            req.body.answer3,
            date,
            time]];

  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("INSERT INTO answers (userID, centerID, questionID, correct, answer, date, time) VALUES ?",[arr],function(err, rows, fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.query("UPDATE users SET flag = ? WHERE id = ? ",[ 2, userID],function(err,rows,fields){
            if(err){
              tempConn.release();
              console.log(err);
              req.flash("error", "Something went wrong");
              res.redirect("back");
            } else{
              tempConn.release();
              req.flash("success", "Successfully completed exam");
              res.redirect("/logout");    
            }
          })    
        }
      })
    }
  })
})
 

router.get("/questions",middleware.checkAdmin, function(req, res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT * FROM questions",function(err,rows){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.render("exams/questions",{questions: rows});    
        }
      })
    }
  })
})

router.post("/questions",middleware.checkAdmin,function(req,res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      var description = req.body.description;
      var answer1 = req.body.answer1;
      var answer2 = req.body.answer2;
      var answer3 = req.body.answer3;
      var answer4 = req.body.answer4;
      var correct = req.body.correct;
      var arr = [description, answer1, answer2, answer3, answer4, correct]
      tempConn.query("INSERT INTO questions (descr, answ1, answ2, answ3, answ4, ransw) VALUES ( ?, ?, ?, ?, ?, ?)",arr,function(err, rows, fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.redirect("/exams/questions");    
        }
      })
    }
  })
})

module.exports = router;      
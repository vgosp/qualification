var express = require("express");
var router  = express.Router();
var mysql   = require("mysql");
var middleware = require("../middleware");

var connection = mysql.createPool({
  connectionLimit: 50,
  host: "localhost",
  user: "root",
  password: "",
  database: "db_project"
})

router.get("/",middleware.checkQual, function(req, res){
   connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT examID FROM placed WHERE userID=?",[req.user.id],function( err, rows, fields ){
        if (err) {
          console.log(err);
          tempConn.release();
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else {
           if(rows.length === 0){
            res.redirect("/");
            tempConn.release();
           } else {
            var date = new Date();
            date = date.getDate()+"-"+date.getMonth()+"-"+date.getFullYear();
            tempConn.query("SELECT flag FROM examination WHERE id=?",[rows[0].examID],function( err, rows, fields ){
              if (err) {
                console.log(err);
                tempConn.release();
                req.flash("error", "Something went wrong");
                res.redirect("back");
              } else {
                if( rows[0].flag===0){
                  tempConn.release();
                  req.flash("error", "Exams have not been started yet");
                  res.redirect("back");
                } else {
                  tempConn.query("SELECT * FROM questions ORDER BY RAND() LIMIT 4",function(err,rows){
                    if(err){
                      tempConn.release();
                      console.log(err);
                      req.flash("error", "Something went wrong");
                      res.redirect("back");
                    } else{
                      tempConn.release();
                      res.render("candidates",{questions: rows});    
                    }
                  })
                }
              }
            })
           }
        }
      })
    }
  }) 
});

router.post("/",middleware.checkAdmin,function(req,res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      var username = req.body.name;
      tempConn.query("INSERT INTO users (name , username, password, role) VALUES (?, ?, ?, ?)",[username,username,"password",3],function(err,rows,fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.redirect("candidates/new");    
        }
      })
    }
  })
})

router.get("/new",middleware.checkAdmin, function(req, res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT * FROM users WHERE role=?",[3],function( err, rows, fields ){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.release();
          res.render("candidates/new",{user: rows});    
        }
      })
    }
  })
})

router.get("/place",middleware.checkCenterAdmin, function(req, res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      tempConn.query("SELECT * FROM users WHERE role = ? and flag = ?",[ 3, 0],function( err, rows, fields ){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          var users = rows;
          tempConn.query("SELECT centername FROM examcenter WHERE  adminID = ?",[ req.user.id ],function(err, rows, fields){
            if(err){
              tempConn.release();
              console.log(err);
              req.flash("error", "Something went wrong");
              res.redirect("back");
            } else {
              tempConn.query("SELECT * FROM examination WHERE  examcenter = ? and flag = ?",[ rows[0].centername, 0 ],function(err, rows, fields){
                if(err){
                  tempConn.release();
                  console.log(err);
                  req.flash("error", "Something went wrong");
                  res.redirect("back");
                } else {
                  tempConn.release();
                  res.render("candidates/place",{exams: rows, users: users});
                }
              })
            }
          })   
        }
      })
    }
  })
})

router.post("/place",middleware.checkCenterAdmin,function(req,res){
  connection.getConnection(function(err, tempConn){
    if(err){
      tempConn.release();
      console.log(err);
      req.flash("error", "Something went wrong");
      res.redirect("back");
    } else {
      var userID = req.body.userID;
      var examID = req.body.examID;
      tempConn.query("INSERT INTO placed ( userID , examID) VALUES ( ?, ?)",[userID, examID],function(err,rows,fields){
        if(err){
          tempConn.release();
          console.log(err);
          req.flash("error", "Something went wrong");
          res.redirect("back");
        } else{
          tempConn.query("UPDATE users SET flag = ? WHERE id = ? ",[ 1, userID],function(err,rows,fields){
            if(err){
              tempConn.release();
              console.log(err);
              req.flash("error", "Something went wrong");
              res.redirect("back");
            } else{
              tempConn.release();
              res.redirect("/candidates/place"); 
            }
          })   
        }
      })
    }
  })
})

module.exports = router;
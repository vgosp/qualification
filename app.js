var express     = require("express"),
    app         = express(),
    session 	= require("express-session")
    bodyParser  = require("body-parser"),
    flash       = require("connect-flash"),
    methodOverride = require("method-override"),
    mysql 		= require("mysql"),
    passport = require('passport'),
    LocalStrategy = require("passport-local"),
    MySQLStrore  = require("express-mysql-session")(session);

    
  
//requiring routes
var centerRoutes    = require("./routes/centers"),
    adminRoutes     = require("./routes/admins"),
    indexRoutes     = require("./routes/index"),
    examRoutes      = require("./routes/exams"),
    candidateRoutes = require("./routes/candidates"),
    resultRoutes	= require("./routes/results"),
    middleware 		= require("./middleware");
    // passportConfig = require("./config/passport");
    
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
app.use(flash());
// seedDB(); //seed the database

var connection = mysql.createPool({
	connectionLimit: 50,
	host: "localhost",
	user: "root",
	password: "",
	database: "projectdb"
})

var options = {
	host: "localhost",
	user: "root",
	password: "",
	database: "projectdb"
}

var sessionStore = new MySQLStrore(options);

// PASSPORT CONFIGURATION
app.use(require("express-session")({
    secret: "Trollolololol",
    store: sessionStore,
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(function(req, res, next){
   res.locals.currentUser = req.user;
   res.locals.error = req.flash("error");
   res.locals.success = req.flash("success");
   next();
});

app.use("/", indexRoutes);
app.use("/admins", adminRoutes);
app.use("/centers", centerRoutes);
app.use("/exams", examRoutes);
app.use("/exams/results", resultRoutes);
app.use("/candidates", candidateRoutes);

app.get("/*", function(req,res){
	res.redirect("/");
})

app.listen(8080,function() {
	console.log("Server is up");
});

